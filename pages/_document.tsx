import Document from "next/document";
import React from "react";
import { createGenerateClassName, JssProvider, SheetsRegistry } from "react-jss";

export default class JssDocument extends Document {
    public static async getInitialProps(ctx) {
        const registry = new SheetsRegistry();
        const generateClassName = createGenerateClassName();
        const originalRenderPage = ctx.renderPage;

        ctx.renderPage = () =>
            originalRenderPage({
                enhanceApp: App => props => (
                    <JssProvider registry={registry} generateClassName={generateClassName}>
                        <App {...props} />
                    </JssProvider>
                ),
            });

        const initialProps = await Document.getInitialProps(ctx);

        return {
            ...initialProps,
            styles: (
                <>
                    {initialProps.styles}
                    <style id="server-side-styles">{registry.toString()}</style>
                </>
            ),
        };
    }
}
