import axios from "axios";
import React from "react";
import {
    Button,
    Card,
    Divider,
    Form,
    Grid,
    Header,
    Icon,
    Image,
    Input,
    Loader,
    Segment,
} from "semantic-ui-react";

import ItemSearch from "$components/ItemSearch";
import IItem, { IRecipe } from "$types/IItem";

interface ISelectedItem extends IItem {
    quantity: number;
}

interface IItemIngredient extends IRecipe {
    unitPriceKnown?: boolean;
    unitPrice: number;
    totalPrice: number;
}

interface IAddItemsPageState {
    itemData: IItem[] | null;
    selection: ISelectedItem[];
    ingredients: { [key: number]: IItemIngredient };
    step: number;
}

const IngredientItem = ({ ingredient, onUnitPriceChange, onTotalPriceChange }) => (
    <Card key={ingredient.id}>
        <Card.Content>
            <Grid verticalAlign="middle">
                <Grid.Row stretched columns="three">
                    <Grid.Column
                        style={{
                            paddingLeft: "0.4rem",
                            paddingRight: 0,
                            textAlign: "right",
                        }}
                        width={3}
                    >
                        <Header size="small">{ingredient.quantity}&nbsp;x</Header>
                    </Grid.Column>
                    <Grid.Column style={{ padding: "0 0.4rem" }} width={4}>
                        <Image
                            style={{
                                backgroundColor: "#eeeeee4a",
                                marginBottom: "0",
                            }}
                            bordered
                            src={ingredient.imgUrl}
                            size="massive"
                        />
                    </Grid.Column>
                    <Grid.Column
                        style={{
                            paddingLeft: 0,
                            paddingRight: "0.4rem",
                        }}
                        width={9}
                    >
                        <Header as="h5">
                            <Header.Content>
                                {ingredient.name}
                                <Header.Subheader>
                                    {ingredient.type} Niv. {ingredient.lvl}
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Card.Content>
        <Card.Content extra>
            <Form>
                <Form.Group widths="equal" style={{ marginBottom: "0" }}>
                    <Form.Field
                        control={Input}
                        style={{
                            borderBottom: `2px solid ${
                                ingredient.unitPriceKnown === true ? "#00a1ffb3" : "#e4e4e4"
                            }`,
                            borderRadius: ".28571429rem",
                        }}
                        fluid
                        type="number"
                        label="Prix unitaire"
                        value={ingredient.unitPrice}
                        onChange={(event, data) => onUnitPriceChange(ingredient.id, data.value)}
                    />
                    <Form.Field
                        control={Input}
                        style={{
                            borderBottom: `2px solid ${
                                ingredient.unitPriceKnown === false ? "#00a1ffb3" : "#e4e4e4"
                            }`,
                            borderRadius: ".28571429rem",
                        }}
                        fluid
                        type="number"
                        label="Prix total"
                        value={ingredient.totalPrice}
                        onChange={(event, data) => onTotalPriceChange(ingredient.id, data.value)}
                    />
                </Form.Group>
            </Form>
        </Card.Content>
    </Card>
);

const ItemItem = ({ item, onRemoveItem, onItemQuantityChange }) => (
    <Card>
        <div>
            <Button
                tabIndex={-1}
                style={{ float: "right" }}
                onClick={() => onRemoveItem(item._id)}
                circular
                icon="close"
                size="mini"
                negative
            />
        </div>
        <Card.Content>
            <Grid verticalAlign="middle">
                <Grid.Row stretched>
                    <Grid.Column>
                        <Header as="h4" image>
                            <div
                                style={{
                                    marginBottom: "0.4rem",
                                    textAlign: "center",
                                    width: "100%",
                                }}
                            >
                                <Image
                                    style={{ backgroundColor: "#eeeeee4a", width: "100px" }}
                                    src={item.imgUrl}
                                    bordered
                                />
                            </div>
                            <Header.Content>
                                {item.name}
                                <Header.Subheader>
                                    {item.type} Niv. {item.lvl}
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row stretched>
                    <Grid.Column>
                        <Form>
                            <Form.Input
                                fluid
                                type="number"
                                label="Quantité"
                                value={item.quantity}
                                onChange={(event, data) =>
                                    onItemQuantityChange(item._id, data.value)
                                }
                            />
                        </Form>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Card.Content>
    </Card>
);

export default class AddItemsPage extends React.Component<{}, IAddItemsPageState> {
    public state: IAddItemsPageState = {
        ingredients: {},
        itemData: null,
        selection: [],
        step: 0,
    };

    public onItemSelection = (result: IItem) => {
        if (!this.state.selection.find(item => item._id === result._id)) {
            this.setState({
                ingredients: this.getAllIngredients([
                    { ...result, quantity: 1 },
                    ...this.state.selection,
                ]),
                selection: [{ ...result, quantity: 1 }, ...this.state.selection],
                step: 1,
            });
        }
    };

    public onRemoveSelection = (id: number) => {
        const newSelection = this.state.selection.filter(item => item._id !== id);
        this.setState({ selection: newSelection, step: newSelection.length ? 1 : 0 });
    };

    public onQuantityChange = (id: number, quantity: number) => {
        const item = this.state.selection.find(it => it._id === id);

        if (item) {
            item.quantity = +quantity;
            this.setState({
                ingredients: this.getAllIngredients(),
            });
        }
    };

    public onIngredientTotalPriceChange = (id: number, value: number) => {
        const ingredient = this.state.ingredients[id];

        if (ingredient) {
            ingredient.totalPrice = value;
            ingredient.unitPrice = ingredient.quantity ? Math.ceil(value / ingredient.quantity) : 0;
            ingredient.unitPriceKnown = false;
            this.forceUpdate();
        }
    };

    public onIngredientUnitPriceChange = (id: number, value: number) => {
        const ingredient = this.state.ingredients[id];

        if (ingredient) {
            ingredient.unitPrice = value;
            ingredient.totalPrice = value * ingredient.quantity;
            ingredient.unitPriceKnown = true;
            this.forceUpdate();
        }
    };

    public getAllIngredients = (selection?: ISelectedItem[]) => {
        const newIngredients: { [key: number]: IItemIngredient } = {};
        const { ingredients } = this.state;

        if (!selection) {
            selection = this.state.selection;
        }

        for (const item of selection) {
            for (const ingredient of item.recipe) {
                if (!newIngredients[ingredient.id]) {
                    newIngredients[ingredient.id] = {
                        ...ingredient,
                        quantity: ingredient.quantity * item.quantity,
                        totalPrice: 0,
                        unitPrice: 0,
                    };
                } else {
                    newIngredients[ingredient.id].quantity += ingredient.quantity * item.quantity;
                }
            }
        }

        const entries = Object.entries(newIngredients);
        for (const [id, data] of entries) {
            if (ingredients[id]) {
                const ing = ingredients[id];

                if (ing.quantity === data.quantity) {
                    data.unitPrice = ing.unitPrice;
                    data.totalPrice = ing.totalPrice;
                    data.unitPriceKnown = ing.unitPriceKnown;
                } else {
                    if (ing.unitPriceKnown === true) {
                        data.unitPrice = ing.unitPrice;
                        data.totalPrice = ing.unitPrice * data.quantity;
                        data.unitPriceKnown = true;
                    } else if (ing.unitPriceKnown === false) {
                        data.unitPrice = data.quantity
                            ? Math.ceil(ing.totalPrice / data.quantity)
                            : 0;
                        data.totalPrice = ing.totalPrice;
                        data.unitPriceKnown = false;
                    }
                }
            }
        }

        return newIngredients;
    };

    public componentWillMount() {
        Promise.all(
            ["weapons", "equipments", "consumables"].map(model =>
                axios.get<IItem[]>(`/static/dofapi/${model}.json`),
            ),
        ).then(([weapons, equipments, consumables]) => {
            this.setState({
                itemData: weapons.data.concat(equipments.data, consumables.data),
            });
        });
    }

    public render() {
        const { itemData, selection, step } = this.state;

        if (!itemData) {
            return <Loader />;
        }

        const ingredients = Object.values(this.state.ingredients);

        return (
            <Segment placeholder>
                <Header icon>
                    <Icon name="search" />
                    Veuillez ajouter des objets
                </Header>
                <ItemSearch<IItem> data={itemData} onSelect={this.onItemSelection} />
                {step >= 1 && selection && (
                    <>
                        <Divider />
                        <Header style={{ marginTop: 0 }} size="medium">
                            Objets sélectionnés:
                        </Header>
                        <Card.Group centered stackable>
                            {selection.map(item => (
                                <ItemItem
                                    key={item._id}
                                    item={item}
                                    onRemoveItem={this.onRemoveSelection}
                                    onItemQuantityChange={this.onQuantityChange}
                                />
                            ))}
                        </Card.Group>
                        {ingredients.length ? (
                            <>
                                <Header size="medium">Ingrédients cumulés:</Header>
                                <Card.Group centered stackable>
                                    {ingredients.map(ingredient => (
                                        <IngredientItem
                                            key={ingredient.id}
                                            ingredient={ingredient}
                                            onTotalPriceChange={this.onIngredientTotalPriceChange}
                                            onUnitPriceChange={this.onIngredientUnitPriceChange}
                                        />
                                    ))}
                                </Card.Group>
                            </>
                        ) : null}
                    </>
                )}
            </Segment>
        );
    }
}
