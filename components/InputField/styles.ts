export default () => ({
    inputWrapper: {
        display: "inline-flex",
        marginTop: "16px",
        position: "relative",
    },
    label: {
        color: "rgba(0, 0, 0, 0.54)",
        fontSize: "1rem",
        left: 0,
        lineHeight: 1,
        padding: 0,
        pointerEvents: "none",
        position: "absolute",
        top: 0,
        transform: "translate(4px, 27px) scale(1)",
        transformOrigin: "top left",
        transition: "transform 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms",
        zIndex: 5,
    },
    labelTop: {
        pointerEvents: "all",
        transform: "translate(0, 1.5px) scale(0.85)",
    },
    outerWapper: {
        display: "inline-flex",
        marginTop: "-16px",
        position: "relative",
    },
});
