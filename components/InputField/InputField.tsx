import React from "react";
import withStyles, { WithSheet } from "react-jss";
import { Input, InputProps } from "semantic-ui-react";

import styles from "./styles";

const tryConvert = val => (isNaN(+val) ? val : +val);

interface IInputFieldProps extends WithSheet<typeof styles> {
    label?: string;
    helperText?: string;
    id: string;
    inputProps: InputProps;
    [key: string]: any;
}

interface IInputFieldState {
    value: string;
    focused: boolean;
}

class InputField extends React.Component<IInputFieldProps, IInputFieldState> {
    public componentWillMount() {
        const { inputProps } = this.props;

        this.setState({
            focused: inputProps.focus || false,
            value: inputProps.defaultValue || inputProps.value,
        });
    }

    public render() {
        const { label, helperText, id, inputProps, classes, ...rest } = this.props;
        const { value, focused } = this.state;

        return (
            <div {...rest} className={classes.outerWapper}>
                {label && (
                    <label
                        className={`${classes.label} ${(focused || value !== "") &&
                            classes.labelTop}`}
                        htmlFor={id}
                    >
                        {label}
                    </label>
                )}
                <div className={classes.inputWrapper}>
                    <Input
                        {...inputProps}
                        onFocus={(...args) => {
                            this.setState({
                                focused: true,
                            });
                            if (inputProps.onFocus) {
                                inputProps.onFocus(...args);
                            }
                        }}
                        onBlur={(...args) => {
                            this.setState({
                                focused: false,
                            });
                            if (inputProps.onBlur) {
                                inputProps.onBlur(...args);
                            }
                        }}
                        onChange={(event, data) => {
                            if (inputProps.onChange) {
                                inputProps.onChange(event, data);
                            }
                            this.setState({ value: tryConvert(data.value) });
                        }}
                        id={id}
                        size="small"
                    />
                </div>
                {helperText && <p id={`${id}-helper-text`}>{helperText}</p>}
            </div>
        );
    }
}

/*
<InputField
    id="helperText"
    label="Helper text"
    defaultValue="Default Value"
    className={classes.textField}
    helperText="Some important text"
    margin="normal"
/>
*/
export default withStyles(styles)(InputField);
