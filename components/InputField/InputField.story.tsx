import { RenderFunction, storiesOf } from "@storybook/react";
import React from "react";
import { ThemeProvider } from "react-jss";

import InputField from "./InputField";

class Wrapper extends React.Component<{ story: RenderFunction }> {
    public render() {
        return this.props.story();
    }
}

const ThemeDecorator = (story: RenderFunction) => (
    <ThemeProvider theme={{}}>
        <Wrapper story={story} />
    </ThemeProvider>
);

const CenterDecorator = (story: RenderFunction) => (
    <div
        style={{
            marginTop: "50px",
            textAlign: "center",
        }}
    >
        {story()}
    </div>
);

storiesOf("InputField", module)
    .addDecorator(CenterDecorator)
    .addDecorator(ThemeDecorator)
    .add("basic", () => (
        <InputField
            label="Quantité"
            id="basic-test"
            inputProps={{
                type: "number",
            }}
        />
    ))
    .add("label up", () => (
        <InputField
            label="Hello"
            id="basic-test"
            inputProps={{
                type: "text",
                value: "toto",
            }}
        />
    ));
