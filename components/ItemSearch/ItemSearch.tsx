import fuzzy from "fuzzysort";
import _ from "lodash";
import React, { Component } from "react";
import { Search, SearchResultProps } from "semantic-ui-react";

import removeAccents from "$helpers/removeAccents";

const ResultRenderer = ({ title, imgUrl }: SearchResultProps) => (
    <div style={{ position: "relative", height: "25px" }}>
        <img
            style={{
                float: "left",
                height: "50px",
                marginTop: "-25px",
                position: "absolute",
                top: "50%",
                width: "50px",
            }}
            src={imgUrl}
        />
        <div
            style={{
                left: "55px",
                position: "absolute",
                top: "50%",
                transform: "translateY(-50%)",
            }}
        >
            {title && <div className="title">{title}</div>}
        </div>
    </div>
);

interface IChoice {
    _id: number;
    title: string;
    imgUrl: string;
    noAccents: string;
}

interface IItemSearchProps<T> {
    data: T[];
    onSelect: (result: T) => void;
}

interface IItemSearchState<T> {
    results: T[];
    value: string;
    isLoading: boolean;
}

const ResultWrapper = params => (
    <div className={params.className} onClick={e => params.onClick(e)}>
        {params.children}
    </div>
);

export default class ItemSearch<T extends IChoice> extends Component<
    IItemSearchProps<T>,
    IItemSearchState<T>
> {
    public state: IItemSearchState<T> = {
        isLoading: false,
        results: [],
        value: "",
    };

    public resetComponent = () => this.setState({ isLoading: false, results: [], value: "" });

    public handleResultSelect = (e: any, { result }: { result: T }) => {
        this.resetComponent();
        this.props.onSelect(result);
    };

    public handleSearchChange = (e: any, { value }: { value: string }) => {
        if (!value) {
            return this.resetComponent();
        }

        this.setState({
            isLoading: true,
            value,
        });

        setImmediate(() => {
            const fuzzyResult = fuzzy.go(removeAccents(value), this.props.data, {
                allowTypo: false,
                key: "noAccents",
                limit: 12,
            });

            this.setState({
                isLoading: false,
                results: fuzzyResult.map(el => ({
                    as: ResultWrapper,
                    childKey: el.obj._id,
                    ...el.obj,
                })),
            });
        });
    };

    public render() {
        const { isLoading, value, results } = this.state;

        return (
            <div style={{ textAlign: "center" }}>
                <div style={{ display: "inline-block" }}>
                    <Search
                        selectFirstResult
                        style={{ textAlign: "center" }}
                        loading={isLoading}
                        size="large"
                        aligned="left"
                        icon={null}
                        placeholder="Ex: Boufbottes"
                        onResultSelect={this.handleResultSelect}
                        onSearchChange={_.debounce(this.handleSearchChange, 5000, {
                            leading: true,
                        })}
                        results={results}
                        resultRenderer={props => <ResultRenderer {...props} />}
                        value={value}
                    />
                </div>
            </div>
        );
    }
}
