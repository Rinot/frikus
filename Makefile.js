const axios = require("axios");
const fs = require("fs");
const _ = require("lodash");
const path = require("path");
const ProgressBar = require("progress");

const staticDir = path.resolve(__dirname, "static");
const dataDir = path.resolve(staticDir, "dofapi");
const imgDir = path.resolve(dataDir, "img");
const models = ["weapons", "equipments", "consumables"];

const removeAccents = str => str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const toInt = str => parseInt(str, 10);

const processDataArray = arr =>
    arr
        .reduce((arr, v) => {
            return arr.concat(Object.entries(v));
        }, [])
        .map(([name, data]) => {
            const tryConvert = val => (isNaN(+val) ? val : +val);

            const res = {
                name,
            };
            if (_.isObject(data)) {
                Object.entries(data).forEach(([key, val]) => {
                    res[key] = tryConvert(val);
                });
            } else {
                res.value = tryConvert(data);
            }

            return res;
        });

const dlAndPipe = (url, file) =>
    axios({
        method: "get",
        url,
        responseType: "stream",
    }).then(response => {
        response.data.pipe(fs.createWriteStream(file));
    });

const handleRecipe = async item => {
    const recipeArray = item.recipe.map(r => Object.values(r)[0]);
    for (let i = 0; i < recipeArray.length; i++) {
        const recipe = recipeArray[i];

        if (!fs.existsSync(path.resolve(imgDir, `${recipe.id}.png`))) {
            try {
                const imgRes = await dlAndPipe(
                    recipe.imgUrl.replace(".w48h48", "").replace("/items/52/", "/items/200/"),
                    path.resolve(imgDir, `${recipe.id}.png`),
                );
                await sleep(200);
            } catch (e) {
                await fs.promises.writeFile("err.json", JSON.stringify(e));
                console.error("Error. See err.json", e.message);
                process.exit(1);
            }
        }

        recipe.imgUrl = `/static/dofapi/img/${recipe.id}.png`;
    }
};

exports.getData = async () => {
    if (!fs.existsSync(dataDir)) {
        await fs.promises.mkdir(dataDir);
    }
    if (!fs.existsSync(imgDir)) {
        await fs.promises.mkdir(imgDir);
    }

    for (let j = 0; j < models.length; j++) {
        const model = models[j];
        console.log("Processing model", model);

        console.log("Retrieving data from dofapi...");
        let start = process.hrtime();
        const response = await axios.get(`https://dofapi2.herokuapp.com/${model}`);
        let end = process.hrtime(start);
        const t = Math.round((end[0] * 1000000000 + end[1]) / 1000000);
        console.log(`Took ${t}ms`);

        process.stderr.isTTY = true;
        process.stderr.columns = 80;
        process.stderr.cursorTo = () => {
            process.stderr.write("\r");
        };
        process.stderr.clearLine = () => {};

        const progressBar = new ProgressBar(`Processing ${model}: :current/:total :percent`, {
            total: response.data.length,
        });

        for (let i = 0; i < response.data.length; i++) {
            const item = response.data[i];

            if (!fs.existsSync(path.resolve(imgDir, `${item._id}.png`))) {
                try {
                    await dlAndPipe(item.imgUrl, path.resolve(imgDir, `${item._id}.png`));
                    await sleep(200);
                } catch (e) {
                    await fs.promises.writeFile("err.json", JSON.stringify(e));
                    console.error("Error. See err.json", e.message);
                    process.exit(1);
                }
            }

            await handleRecipe(item);

            item.imgUrl = `/static/dofapi/img/${item._id}.png`;
            item.title = item.name;
            item.lvl = toInt(item.lvl);
            item.stats = processDataArray(item.stats);
            item.recipe = processDataArray(item.recipe);
            item.noAccents = removeAccents(item.title);

            if (item.characteristic) {
                item.characteristic = processDataArray(item.characteristic);
            }

            progressBar.tick();
        }

        await fs.promises.writeFile(
            path.resolve(dataDir, `${model}.json`),
            JSON.stringify(response.data),
        );

        console.log("Done");
    }
};

exports["retrieve-data"] = async url => {
    const https = require("https");
    const extract = require("extract-zip");
    const localMediaPath = path.resolve(staticDir, "data.zip");

    const download = (url, dest) =>
        new Promise((resolve, reject) => {
            const file = fs.createWriteStream(dest);

            https
                .get(url, response => {
                    response.pipe(file);

                    file.on("finish", () => {
                        file.close((err, res) => {
                            if (err) {
                                return reject(err);
                            }

                            resolve(res);
                        });
                    });
                })
                .on("error", err => {
                    fs.unlink(dest);

                    reject(err);
                });
        });

    console.log("Downloading file");
    await download(url, localMediaPath);

    console.log("Unzipping data");
    await new Promise((resolve, reject) => {
        extract(localMediaPath, { dir: staticDir }, err => {
            if (err) {
                return reject(err);
            }

            resolve();
        });
    });
    console.log("All done.");
};
