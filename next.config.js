// @ts-check

const withTypescript = require("@zeit/next-typescript");
const withPlugins = require("next-compose-plugins");
const withProgressBar = require("next-progressbar");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const { ANALYZE } = process.env;
const path = require("path");

module.exports = withPlugins([withProgressBar, withTypescript], {
    webpack: function(config) {
        if (ANALYZE) {
            config.plugins.push(
                new BundleAnalyzerPlugin({
                    analyzerMode: "server",
                    analyzerPort: 8888,
                    openAnalyzer: true,
                }),
            );
        }

        config.resolve.alias.$components = path.join(__dirname, "components");
        config.resolve.alias.$helpers = path.join(__dirname, "helpers");
        config.resolve.alias.$types = path.join(__dirname, "types");

        return config;
    },
});
