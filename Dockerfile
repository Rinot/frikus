FROM node:9.5.0-alpine
LABEL author="Telokis"

WORKDIR /app

ENV NODE_ENV production
EXPOSE 80

ADD ./.next/ /app/.next
ADD ./start.sh/ /app/start.sh
ADD ./static/ /app/static
ADD ./Makefile.js/ /app/Makefile.js
ADD ./package.json /app/package.json
ADD ./package-lock.json /app/package-lock.json

RUN cd /app ; apk --no-cache add --virtual native-deps \
  git g++ gcc libgcc libstdc++ linux-headers make python && \
  npm install node-gyp npm -g && \
  npm ci && \
  npm cache clean --force && \
  apk del native-deps

ENTRYPOINT ["/bin/sh"]
CMD ["/app/start.sh"]
